# vagrant 是什么

- 这是一个**快速创建虚拟机的工具**，能帮助我们快速的创建不同版本的虚拟机。支持多个版本的操作系统：mac、windows。比如可以通过vagrant 相关命令 给 virtual box 、vmware 创建出虚拟机。默认不配置是给virtual box 创建虚拟机。

- 拿VirtualBox举例，VirtualBox会开放一个创建虚拟机的接口，Vagrant会利用这个接口创建虚拟机，并且通过Vagrant来管理，配置和自动安装虚拟机。

# 相关网址

[vagrant cloud](https://app.vagrantup.com/boxes/search) 可以查到vagrant 提供的box

![image-20200603171327534](vagrant的使用以及安装docker.assets/image-20200603171327534.png)

[vagrant download](https://www.vagrantup.com/)

[virtual box download](https://www.virtualbox.org/wiki/Downloads)

[vagrant document](https://www.vagrantup.com/docs)

# 环境要求

- 安装对应操作系统版本的Vagrant
- 安装对应操作系统版本的Virtual Box

# vagrant 命令



## Vagrantfile 相关

```shell
# vagrant init [options] [name [url]]

# 不指定box 的名字，需要后面修改Vagrantfile 指定box 的信息（可以去vagrant cloud 查看）
$ vagrant init

# 指定box 的名字，这个名字必须是vagrant cloud 里面有的，乱写是没用的
$ vagrant init centos/7

# vagrant init 的效果是在当前文件夹下面创建出一个Vagrantfile 的文件，
# 这个文件使我们启动的虚拟机的配置文件。
```



## box相关

```shell
# 自己下载好之后，添加到vagrant 中，添加到vagrant 的名字 centos/7 ， 添加的是哪个box(推荐使用这种方式)
$ vagrant box add centos/7 /Users/haitao/Downloads/virtualbox.box
# 查看当前安装了那些box
$ vagrant box list 
# 删除vagrant box
$ vagrant box remove
```



## 虚拟机相关

```shell
# 根据当前目录下的Vagrantfile 启动虚拟机
$ vagrant up

# 根据当前目录下的Vagrantfile 启动虚拟机,并执行provision里面的指令
# 因为里面的指令只有在第一次up 的时候会执行。
$ vagrant up --provision

# 根据当前目录下的Vagrantfile 进入虚拟机（交互模式）
$ vagrant ssh

# 根据当前目录下的Vagrantfile 关闭虚拟机
$ vagrant halt

# 根据当前目录下的Vagrantfile 暂停虚拟机
$ vagrant suspend

# # 根据当前目录下的Vagrantfile 销毁虚拟机
$ vagrant destroy

# 根据当前目录下的Vagrantfile 重启虚拟机
$ vagrant reload

# 根据当前目录下的Vagrantfile 重启虚拟机的时候执行Vagrantfile 里面的provision 的指令
# 默认是vagrant up 的时候启动一次
$ vagrant reload 一provision

# 执行Vagrantfile 里面的provision 的指令
$ vagrant provission

# 查看当前虚拟机的状态
$ vagrant status

# 打包虚拟机
$ vagrant package --base xx --output ./xx.box
```

# Vagrantfile 文件的说明

```ruby
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
 
  # box 的标识，可以去这个网址查看
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "centos/7"

  
  # 是否检查更新box
  # config.vm.box_check_update = false

  # 端口转发，将虚拟机的80，映射成物理主机的8080
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # 端口转发还可以指定 映射到物理主机的那个ip上
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # 这个可以理解成设置静态ip，ip随便怎么写都可以。
  config.vm.network "private_network", ip: "192.168.66.10"
  
  # 这个也是静态ip，但是设置了这个选项，执行vagrant up 的时候，会让你选择使用物理主机的那个网卡作用桥接的网卡。
  # 然后我们设置的ip 必须使我们选择的网卡的网段内的ip地址
  # config.vm.network "public_network",ip:"192.168.10.110"

  # 挂载目录到虚拟机中
  # config.vm.synced_folder "../data", "/vagrant_data"

 
  # 设置 虚拟机提供商的信息，默认是virtualbox。就是指定给那个虚拟机软件创建虚拟机
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  
  # 虚拟机启动的时候会执行里面的命令
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL
end

```



# 离线下载vagrant box

[参考链接](https://c4ys.com/archives/1230)

## 国内镜像下载

- CentOS 6/7/8 修改链接中的版本号可以下载不同版本 http://mirrors.ustc.edu.cn/centos-cloud/centos/6/vagrant/x86_64/images/
- Ubuntu Server 14.04 https://mirrors.ustc.edu.cn/ubuntu-cloud-images/server/vagrant/trusty/current/
- Ubuntu Server 16.04 https://mirrors.ustc.edu.cn/ubuntu-cloud-images/server/xenial/current/
- Ubuntu Server 18.04 https://mirrors.ustc.edu.cn/ubuntu-cloud-images/server/bionic/current/
- Ubuntu Server 20.04 http://mirrors.ustc.edu.cn/ubuntu-cloud-images/focal/current/

## 官方直接下载

- Debian 8 https://app.vagrantup.com/debian/boxes/jessie64/versions/8.11.1/providers/virtualbox.box
- Debian 9 https://app.vagrantup.com/debian/boxes/stretch64/versions/9.12.0/providers/virtualbox.box
- Debian 10 https://app.vagrantup.com/debian/boxes/buster64/versions/10.4.0/providers/virtualbox.box
- laravel/homestead https://app.vagrantup.com/laravel/boxes/homestead/versions/9.5.1/providers/virtualbox.box

## 将离线下载好的box安装到vagrant

```shell
$ vagrant box add centos/7 /Users/haitao/Downloads/virtualbox.box
```

# 实操

1. 添加离线下载好的box 到vagrant中

   ```shell
   $ vagrant box add centos/7 /Users/haitao/Downloads/镜像/virtualbox.box
   ==> box: Box file was not detected as metadata. Adding it directly...
   ==> box: Adding box 'centos/7' (v0) for provider:
       box: Unpacking necessary files from: file:///Users/haitao/Downloads/%E9%95%9C%E5%83%8F/virtualbox.box
   ==> box: Successfully added box 'centos/7' (v0) for 'virtualbox'!
   ```

2. 创建一个空目录，并执行vagrant init

   ```shell
   $ mkdir -p  /opt/module/vagrantFile/centos7-01
   
   $ cd /opt/module/vagrantFile/centos7-01
   
   # 这个 cenntos/7 指的是本地vagrant box 中的box 的名字，如果本地不存在就回去vagrant cloud 寻找，找到就下载
   $ vagrant init cenntos/7
   A `Vagrantfile` has been placed in this directory. You are now
   ready to `vagrant up` your first virtual environment! Please read
   the comments in the Vagrantfile as well as documentation on
   `vagrantup.com` for more information on using Vagrant.
   ```

3. 修改Vgrantfile，设置静态ip，启动的时候就安装docker(**注意我里面的阿里云镜像加速地址已删除，自己注册阿里云账号就能获得一个唯一的**)

   ```shell
   # -*- mode: ruby -*-
   # vi: set ft=ruby :
   # All Vagrant configuration is done below. The "2" in Vagrant.configure
   # configures the configuration version (we support older styles for
   # backwards compatibility). Please don't change it unless you know what
   # you're doing.
   Vagrant.configure("2") do |config|
    
     config.vm.box = "centos/7"
   
    
     # config.vm.box_check_update = false
     # config.vm.network "forwarded_port", guest: 80, host: 8080
     # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"
   
     
     # 可以理解成静态ip，ip的设置随便写，vagrant 的会帮我们处理网络的问题
     config.vm.network "private_network", ip: "102.3.2.100"
   
     # 可以理解成静态ip，但是设置了这个会在启动虚拟机的时候，要求你指定使用本机的那个网卡进行桥接网络。此时我们设置的静态ip必须是网卡的子网范围内的
     # config.vm.network "public_network"
     # config.vm.network "public_network",ip:"192.168.10.110"
     # config.vm.network "public_network",ip:"192.168.8.125"
   
     # 挂载目录到虚拟机中
     # config.vm.synced_folder "../data", "/vagrant_data"
   
     # 定制虚拟机的信息
     # config.vm.provider "virtualbox" do |vb|
     #   # Display the VirtualBox GUI when booting the machine
     #   vb.gui = true
     #
     #   # Customize the amount of memory on the VM:
     #   vb.memory = "1024"
     # end
     
     # 编写开启的时候执行的命令 
     # config.vm.provision "shell", inline: <<-SHELL
     #   apt-get update
     #   apt-get install -y apache2
     # SHELL
     config.vm.provision "shell", inline: <<-SHELL
   
         # 设置阿里centos 软件源
         sudo yum install -y wget
         sudo cp /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
         # sudo curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
   			wget http://mirrors.aliyun.com/repo/Centos-7.repo
   			sudo mv Centos-7.repo /etc/yum.repos.d/CentOS-Base.repo
         sudo yum  clean all
         sudo yum  makecache
         sudo yum -y update
   
         # 删除以前的版本
         sudo yum -y remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine
         # docker 需要安装的包，已经常用的软件
         sudo yum install -y yum-utils vim net-tools ssh openssh-server
   
         # 设置镜像的仓库
         sudo yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
   
         # 安装docker  docker-ce 这是社区版
         sudo yum install -y docker-ce docker-ce-cli containerd.io
   
         # 阿里云镜像加速,这个需要自己弄一个（注册阿里云）
         touch xx
         if [[ ! -d /etc/docker ]];then  sudo mkdir /etc/docker; fi
         echo '{"registry-mirrors": ["https://xxxx.mirror.aliyuncs.com"]}' > xx
         sudo mv xx /etc/docker/daemon.json
   
         sudo systemctl daemon-reload
         sudo systemctl start docker
   
       SHELL
   end
   
   ```

4. 使用vagrant 启动虚拟机

   ```shell
   $ vagrant up
   ```

5. 测试docker 是否成功安装

   ```shell
   [vagrant@localhost ~]$ sudo docker run hello-world
   ```

   ![image-20200603194754119](vagrant的使用以及安装docker.assets/image-20200603194754119.png)

6. 测试主机和虚拟机之间的交互

   ![image-20200603194955171](vagrant的使用以及安装docker.assets/image-20200603194955171.png)

# vagrant up 报错：无法挂载目录

## mount: unknown filesystem type 'vboxsf'

```shell
# 这是因为vagrant 缺少了插件，安装一下就可以了
$ vagrant plugin install vagrant-vbguest
```



![image-20200604162327884](vagrant的使用以及安装docker.assets/image-20200604162327884.png)

# vagrant plugin

```shell
# 查看本地的插件
$ vagrant plugin list

# 安装插件
$ vagrant plugin install vagrant-vbguest
$ vagrant plugin install vagrant-scp
$ vagrant plugin install vagrant-share
```

# 打包自己的虚拟机成box，并添加到vagrant中

- 比如我们使用了vagrant 官方提供的box 创建出虚拟机，但是这个虚拟机是mini版很多东西都没有。
- 所以我们可以先启动了官方的虚拟机，然后在里面安装很多需要的软件，然后将这个虚拟机打包，再添加到本地的vagrant中

1. 从vagrant cloud 创建虚拟机（物理主机）

   ```shell
   $ vagrant init centos/7
   $ vagrant up
   ```

   ![image-20200605211016657](vagrant的使用以及安装docker.assets/image-20200605211016657.png)

2. 进入创建好的虚拟机，安装我们需要的软件（虚拟机内）

   ```shell
   # vagrant ssh 进入虚拟机，然后在虚拟机里面创建一个文件，文件的内容就是安装软件的命令，然后执行脚本
   $ vagrant ssh
   [vagrant@localhost ~]$ vim setup.sh
   [vagrant@localhost ~]$ cat setup.sh
   ```

   ```shell
   # 执行的安装命令
   # 添加vagrant 到docker 组中，获得docker 的权限
   sudo groupadd docker
   sudo usermod -aG docker vagrant
   
   sudo yum makecache fast
   sudo yum -y update
   
   # 删除以前的版本
   sudo yum -y remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine
   # docker 需要安装的包，已经常用的软件
   # 测试 进程是否能访问 telnet 192.168.0.1 8080
   sudo yum install -y yum-utils vim net-tools ssh openssh-server telnet
   
   # 设置镜像的仓库
   sudo yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
   
   # 安装docker  docker-ce 这是社区版
   sudo yum install -y docker-ce docker-ce-cli containerd.io
   
   # 阿里云镜像加速,这个需要自己弄一个
   touch xx
   if [[ ! -d /etc/docker ]];then  sudo mkdir /etc/docker; fi
   echo '{"registry-mirrors": ["https://yohi7zsq.mirror.aliyuncs.com"]}' > xx
   sudo mv xx /etc/docker/daemon.json
   
   # 安装docker-compose
   sudo curl -L "https://get.daocloud.io/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
   sudo chmod +x /usr/local/bin/docker-compose
   
   sudo systemctl daemon-reload
   sudo systemctl start docker
   
   sudo reboot
   ```

   

   ![image-20200605212235360](vagrant的使用以及安装docker.assets/image-20200605212235360.png)

4. 设置能使用密码登录（虚拟机内）

   ```shell
   [vagrant@localhost ~]$ sudo su
   [root@localhost vagrant]# vim /etc/ssh/sshd_config
     # 打开38行的注释（这的意思是可以使用root 用户登录）
     PermitRootLogin yes
     
   	# 注释掉 66行，改成yes （这的意思是可以通过密码登录）
   	# PasswordAuthentication no
   	PasswordAuthentication yes
   ```

5. 测试是否能通过密码登录（物理主机）

   ```shell
   # vagrant 创建的虚拟机默认的密码都是vagrant
   $ ssh root@127.0.0.1 -p 2222
   root@127.0.0.1's password:
   Last login: Fri Jun  5 13:34:39 2020 from 10.0.2.2
   [root@localhost ~]# exit
   logout
   Connection to 127.0.0.1 closed.
   
   $ ssh vagrant@127.0.0.1 -p 2222
   vagrant@127.0.0.1's password:
   Last login: Fri Jun  5 13:35:24 2020 from 10.0.2.2
   ```

   ![image-20200605213702590](vagrant的使用以及安装docker.assets/image-20200605213702590.png)

6. 此时我们的虚拟机已经安装了docker，并且修改了能通过密码登录，我们将这个虚拟机打包，添加到vagrant中

   ```shell
   $ vagrant status
   $ vboxmanage list vms # 这是virtualbox 的命令
   $ vagrant package --base demo_default_1591362413851_60920 --output ./centos7-docker.box
   $ vagrant box add centos7-docker ./centos7-docker.box
   $ vagrant box list
   ```

   ![image-20200605214823068](vagrant的使用以及安装docker.assets/image-20200605214823068.png)

7. 使用我们刚才自己生成的box 创建出虚拟机

   ```shell
   $ vagrant init centos7-docker
   $ vim Vagrantfile
   ```

   Vagrantfile 文件内容

   ```ruby
   # -*- mode: ruby -*-
   # vi: set ft=ruby :
   
   Vagrant.require_version ">= 1.6.0"
   
   # 定义变量方便扩展
   boxes = [
       {
           :name => "swarm-manager",
           :eth1 => "192.168.2.100",
           :mem => "1024",
           :username => "vagrant",
           :password => "vagrant"
       }
   ]
   
   Vagrant.configure(2) do |config|
   
     config.vm.box = "centos7-docker"
   
     boxes.each do |opts|
         config.vm.define opts[:name] do |config|
           config.vm.hostname = opts[:name]
           config.vm.provider "vmware_fusion" do |v|
             v.vmx["memsize"] = opts[:mem]
             v.vmx["numvcpus"] = opts[:cpu]
           end
   
           config.vm.provider "virtualbox" do |v|
             v.customize ["modifyvm", :id, "--memory", opts[:mem]]
             v.customize ["modifyvm", :id, "--cpus", opts[:cpu]]
           end
   
           config.vm.network :private_network, ip: opts[:eth1]
           config.ssh.username = opts[:username]
           config.ssh.password = opts[:password]
           config.ssh.insert_key = true
         end
     end
   
     # config.vm.synced_folder "./labs", "/home/vagrant/labs"
     # config.vm.provision "shell", privileged: true, path: "./setup.sh"
     config.vm.provision "shell", inline: <<-SHELL
         sudo systemctl stop firewalld
         sudo systemctl start docker
     SHELL
   end
   ```

   ![image-20200605220619047](vagrant的使用以及安装docker.assets/image-20200605220619047.png)

8. 进入虚拟机测试一下

   ![image-20200605221451232](vagrant的使用以及安装docker.assets/image-20200605221451232.png)

