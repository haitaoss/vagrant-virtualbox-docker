# 文件说明

- **vagrant的使用以及安装docker.md** ：这是详细的学习文档

- **centos7.box** ：这是vagrant cloud 提供的box，我已经下载到本地了

- **centos7-docker.box** ：这是我自定义的box，里面安装了docker，docker-compose，并且设置的centos7国内软件镜像源以及docker的国内镜像源
- **两个box下载连接** ：链接:https://pan.baidu.com/s/1ou6FqrddXJwrh-E7V1LFWA  密码:40q1

- **Vagranfile模板** ：里面定义了常用的Vagrantfile 的常用设置，可以根据这个修改。注意：里面使用的box是我定制的，如果需要换成别的自行修改。